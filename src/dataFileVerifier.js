window.wmPreviewSnippet = {
    getServerSettings: ()=> { return {} }
};

window._makeTutorial = {
    appendDataPackage: verifyData,
    dataFileLoaded: () => {}
};

_currentResult = false;

function verifyDataFile(fileStr) {
    _currentResult = false;

    eval(fileStr);

    return _currentResult;
}

function verifyData(data) {
    _currentResult = Object.keys(data).reduce((acc, key)=> {
        return acc && verifyKey(data, key);
    }, true);
}

function verifyKey(data, key) {
    return window.whiteList.includes(key) || data[key].length == 0;
}

