const loadingClass = "loading";
const verifiedClass = "verified";
const failedClass = "failed";
const verificationDivSelector = ".verification-results";

function cleanDisplay() {
    jQuery(verificationDivSelector).empty();
    jQuery('body').removeClass(verifiedClass);
    jQuery('body').removeClass(failedClass);
}

function displayLoading(file, index) {
    jQuery(verificationDivSelector).append(`<div class="${index} loading">loading ${file.name}...</div>`);
}

function displayResult(originalFile, index, result) {
    let displayElement = getDisplayElementByIndex(index);

    displayElement.removeClass(loadingClass);

    if (result) {
        displayElement.addClass(verifiedClass);
        displayElement.text(`No PII-related features are enabled for: ${originalFile.name}`);
    } else {
        displayElement.addClass(failedClass);
        displayElement.text(`PII-related features were found in: ${originalFile.name}`);
    }

}

function displayError(originalFile, index) {
    let displayElement = getDisplayElementByIndex(index);

    displayElement.removeClass(loadingClass);
    displayElement.addClass(failedClass);
    displayElement.text(`file format error: ${originalFile.name}`);
}

function getDisplayElementByIndex(index) {
    return jQuery(`${verificationDivSelector} .${index}`);
}

function displayFinalResult(results) {
    if (results.length === 0 || results.includes(false)) {
        jQuery('body').addClass(failedClass);
    } else {
        jQuery('body').addClass(verifiedClass);
    }
}