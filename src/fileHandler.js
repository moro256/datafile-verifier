async function verifyInput() {
    cleanDisplay();
    const dataFiles = document.getElementById('data-file-input').files;
    let resultPromises = jQuery.map(dataFiles, handleFile);
    let results = await Promise.all(resultPromises);
    displayFinalResult(results);
}

async function handleFile(file, index) {
    try {
        displayLoading(file, index);
        let fileStr = await readFile(file);
        let verificationResult = verifyDataFile(fileStr);
        displayResult(file, index, verificationResult);
        return verificationResult;
    }
    catch(e) {
        displayError(file, index,  e);
        return false;
    }
}

function readFile(file) {
    const reader = new FileReader();

    return new Promise((resolve, reject) => {
        reader.onerror = function () {
            reject(`could not load ${file.name}`);
        };

        reader.onloadend = function () {
            resolve(reader.result);
        };

        reader.readAsText(file);
    });
}
